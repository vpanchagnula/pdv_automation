import subprocess
import hashlib
import  os
import difflib
import hashlib


# returns output as byte string

# filename(open any file and check the key value pair YAML) and  config check
# printing runtime property check with a diff
# check sum of xmls comparing values with the YAML to xml checksum


#  Function returns a list
def create_property_file(serverconfig,primaryconfig,folderpath,logger):
    propertyFile_path = {}
    serverconfig.update(primaryconfig)
    for eachname, portnumb in serverconfig.items():
        logger.info("Iterating through server list and creating a property file for {0}".format(eachname))
        propertyFileName = "{0}/{1}.txt".format(folderpath,eachname)
        #print("execJava com.healthedge.management.client.ConfigurationManagerClient -l t3://{0}:{1}  -c printProperties > {2}".format(eachname,portnumb,propertyFileName))
        os.popen("execJava com.healthedge.management.client.ConfigurationManagerClient -l t3://{0}:{1}  -c printProperties > {2}".format(eachname,portnumb,propertyFileName))
        # Need to write a test for file exists to confirm the success
        propertyFile_path[eachname] = propertyFileName
    logger.info("Created all the property files based on the server name ")
    return  propertyFile_path


# by default all the weblogic01 is primary and then the secondary follows
# remove all the dots from the wlproperty file check property file for more info

def diff_property_File(primaryserverpath,rtp_path):
    diff_in_files = []
    primary_path = {}
    #logger.info("Getting primary and secondary paths ")
    for path in rtp_path.keys():
        if path in primaryserverpath.keys():
            primary_path = rtp_path[path]
    rtp_path.pop(primaryserverpath.keys()[0])
    if isinstance(rtp_path,dict) == True:
        for eachservservername, path in rtp_path.items():
            diff_subprocess = subprocess.Popen( ['diff', str(primaryserverpath), str(path)],  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            get_subprocess_output = diff_subprocess.communicate()
            diff_in_files.append(get_subprocess_output[0].decode("UTF-8"))
    else:
        print("Incorrect parameter given")


    return  diff_in_files


def get_checksum_md5(file_name):
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

# Need to shift this to the main script:





