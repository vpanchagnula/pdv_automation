import errno
import os
import re
import shlex
import shutil
import subprocess
import yaml
from datetime import datetime
import xml.etree.ElementTree as ET


def get_Server_info(input_yaml,logger):
    """
    :param input_yaml:
    :param yaml_type:
    :param logger:
    :return: This function returns a result , primary/secondary servers for weblogic and port numbers
    """
    result = []
    primary_server = {}
    list_servers =[]
    keys_to_search = ('portNumber','secondary')
    logger.info("get_server_info function running")
    for k,v in input_yaml.items():
        result.append(v)
        for f, val in v.items():
            if val.has_key('portNumber'):
                primary_server[f] = val.get("portNumber")
            if val.has_key('Secondary'):
                list_servers = val.get("Secondary")
    logger.info("get_server_info function returning result ")
    return result,primary_server,list_servers


#This is a redundant function can be remved in the future no references.
def getportnumber(serverconfig,logger):
    checksum_dict = {}
    for server,config in serverconfig.items():
        for servername , portnumber in config.items():
            checksum_dict[servername] = portnumber.get('port')

    return  checksum_dict





# will have type as parameter
def getfilename(yaml_details,logger):
    filenameresult = []
    logger.info("getserver function called for necessary output")
    for item in yaml_details:
        for k, v in item.items():
            for file, value in v.items():
                if "file" in file:
                    filenameresult.append(value.get('filename'))
                elif file == 'checksum':
                    for e in value.keys():
                        filenameresult.append(e)

    return filenameresult

#This function will have  capability as a paramter so that i can change dynamically
# servername and filename add to parameter
def get_server_config(yaml_details,filenames,logger):
    logger.info("gerserverconfig function triggered")
    finalresult = {}
    for item in yaml_details:
        for k, v in item.items():
             for file, value in v.items():
                 if "file" in file:
                     for eachfile in filenames:
                        if eachfile == value.get("filename"):
                            for filename, prop in value.items():
                                if filename == 'check':
                                    finalresult[eachfile] = prop

    return finalresult

def get_checksum(yaml_details):
    #logger.info()
    checksum_list =[]
    for item in yaml_details:
        for k,v in item.items():
            for file, value in v.items():
                if file == 'checksum':
                    checksum_list.append(value)
        return checksum_list


def get_just_filename(fullpath):
    path_index = fullpath.rfind("/", 0, len(fullpath))
    filename = fullpath[(path_index + 1):len(fullpath)]
    return filename






