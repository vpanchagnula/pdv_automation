import logging

# need to implement this in all the scripts after POC
# have different logger modes so that we can show different logger outputs

def get_logger(log_file_name):
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s.%(msecs)06d - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    fh = logging.FileHandler(log_file_name, mode='w')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    my_logger.addHandler(ch)
    my_logger.addHandler(fh)

    return my_logger