import os.path
import socket
import propertyFileParser
import YAMLUtility
import time
import yaml
import logging
import sys


# def gloabal varibales do not change:
def get_logger(log_file_name):
    my_logger = logging.getLogger()
    my_logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s.%(msecs)06d - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    fh = logging.FileHandler(log_file_name, mode='w')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    my_logger.addHandler(ch)
    my_logger.addHandler(fh)
    return my_logger


def isFileExist(paths):
    # add empty path constraint after POC
    valid_paths = []
    invalid_paths = []
    for eachPath in paths:
        if os.path.isfile(eachPath):
            logger.info("File exist at path , " + eachPath)
            valid_paths.append(eachPath)
        else:
            invalid_paths.append(eachPath)
    if len(invalid_paths) != 0 :
        logging.info('*********************************')
        logger.critical("ERROR:FILES PATH DO NOT EXIST\n")
        for p in invalid_paths:
            logger.critical("{0}\n".format(p))

    return valid_paths,invalid_paths



# add logger to this function.
def readConfig(path):
    """

    :param path:
    :return:
    """
    conf = {}
    with open(path) as fp:
        lines = (line.rstrip() for line in fp)
        lines = list(line for line in lines if line)  # Non-blank
        for line in lines:
            if line.startswith('#') or line.count("=") >1:
                continue
            key, val = line.split('=')
            conf[key.strip()] = str(val)

    fp.close()
    return conf


def configcompare(all_server_config):
    total_missing_pdv = {}
    total_pdv = {}
    total_missing_prop= {}
    for filepath,config in all_server_config.items():
        validpdv = {}
        missingvalues = {}
        missingproperty = []
        try:
            sourceconfig = readConfig(filepath)
            #print(sourceconfig)
            #print(config)
            for k, v in config.items():
                if k in sourceconfig.keys():
                    # do all the matches to lower and get rid of all white spaces
                    for each in sourceconfig.keys():
                        if each== k:
                            if sourceconfig.get(each).strip()== v.strip() :
                                validpdv[k] = v
                            else:
                                missingvalues[k] = v
                else:
                    missingproperty.append(k)
            if len(missingproperty) !=0:
                total_missing_prop[filepath] = missingproperty
            if len(validpdv) != 0:
                total_pdv[filepath] = validpdv
            if len(missingvalues) != 0:
                total_missing_pdv[filepath] = missingvalues


            if len(total_missing_prop) != 0:
                logging.info('*********************************')
                logger.critical("ERROR:FILES EXISTS BUT PROPERTY MISSING\n")
                for file, property in total_missing_prop.items():
                    logger.critical("{0}:{1}\n".format(file, property))

        except Exception as e:
            logger.critical(e)

    if len(total_missing_pdv) != 0:
        logging.info('*********************************')
        logger.critical("ERROR:FILES AND PROPERTY EXISTS BUT VALUE IS DIFFERENT\n")
        for file, property in total_missing_pdv.items():
            logger.critical("{0}:{1}\n".format(file, property))
    if len(total_pdv) != 0:
        logging.info('*********************************')
        logger.info("FILES AND PROPERTY ARE SAME PDF IS FINE \n")
        for filename, value in total_pdv.items():
            logger.info("{0}:{1}\n".format(filename, value))

    return total_missing_prop,total_missing_pdv,total_pdv




# this function just gets the base YAML without any modifications
def getfile(path,filename):
    with open(os.path.join(path,filename)) as file:
        logger.info("Locating the YAML file")
        yaml_source = yaml.load(file)
        for bcaz,servers in yaml_source.items() :
             return servers

    file.close()


def create_pdv_result(hostname,file_path,file_name,total_filename_count,check_fileexist,check_prop_exist,check_value_exist,total_result,check_xml,check_rtp):
    file = open(os.path.join(file_path,file_name),"w")
    file.write("**********PDV STATUS REPORT:{0}***********\n".format(hostname))
    file.write("\n")
    file.write("\n")
    file.write("***********************************************\n")
    file.write("CHECK: FILE MISSING Report \n")
    if len(check_fileexist) != 0:
        for each in check_fileexist:
            file.write(YAMLUtility.get_just_filename(each)+"\n")
    else:
        file.write("No Errors Found\n")
    if total_filename_count > len(check_fileexist):
        file.write("\n")
        file.write("\n")
        file.write("***********************************************\n")
        file.write("CHECK: FILE EXISTS BUT PROPERTY MISSING\n")
        if len(check_prop_exist) != 0:
            for filename, property in check_prop_exist.items():
                #file.write(YAMLUtility.get_just_filename(filename)+":"+str(property)+"\n")
                file.write("----------->" + YAMLUtility.get_just_filename(filename) + "--" + "\n")
                if len(property) >1:
                    for each in property:
                        file.write(str(each) + "\n")
                else:
                    file.write(str(property) + "\n")
        else:
            file.write("No Errors Found\n")
        file.write("\n"+"\n")
        file.write("***********************************************\n")
        file.write("CHECK: FILE AND PROPERTY EXISTS BUT VALUE DIFFERENT\n")
        if len(check_value_exist) != 0:
            for files, values in check_value_exist.items():
                #file.write(YAMLUtility.get_just_filename(files) + "--"+"\n" + str(values) + "\n")
                file.write("----------->" + YAMLUtility.get_just_filename(files) + "--" + "\n")
                for each in values.items():
                    file.write(str(each) + "\n")

        else:
            file.write("***********************************************\n")
            file.write("No Errors Found\n")
        file.write("\n"+" \n")
        file.write("**********************************************\n")
        file.write("MATCHES YAML:\n")
        if len(total_result) != 0 :
            for f, total in total_result.items():
                file.write("----------->"+ f + "--" + "\n")
                for each in total.items():
                    file.write(str(each) + "\n")

        file.write("\n"+" \n")
        file.write("***********************************************\n")
        file.write("CHECK: CHECKSUM MATCH\n")
        if len(check_xml) !=0:
            for path,result in check_xml.items():
                file.write(path + "--"+str(result)+ "\n")

        file.write("\n"+" \n")
        file.write("***********************************************\n")
        file.write("CHECK: RTP MATCH\n")
        if len(check_rtp) !=0:
            for each in check_rtp:
                file.write(str(each))


    else:
        file.write("100% FAILURE CHECK YAML!!!!!!")
    file.close()

def diff_checksum(checksum_yaml,valid_path):
    result_checksum = {}
    for eachfile in checksum_yaml:
        for xmlpath,checksum  in eachfile.items():
            for each in valid_path:
                if str(xmlpath) ==str(each):
                    checksum_value = propertyFileParser.get_checksum_md5(xmlpath)
                    if checksum_value == checksum:
                        result_checksum[YAMLUtility.get_just_filename(xmlpath)] = "CHECKSUM MATCHED "
                    else:
                        result_checksum[YAMLUtility.get_just_filename(xmlpath)] = "CHECKSUM UNMATCHED "

    return result_checksum




def main():
    # add logic to find dictionary in the tuple so that the hard coding can be removed
    input_file_yaml = getfile(yaml_folder_path, yaml_file)
    get_server_def = YAMLUtility.get_Server_info(input_file_yaml,logger)
    primary_server_weblogic = get_server_def[1]
    secondary_server_weblogic = get_server_def[2]

    # This will work on production:
    get_filenames_yaml = YAMLUtility.getfilename(get_server_def[0], logger)
    get_isfileexist_result = isFileExist(get_filenames_yaml)
    valid_file_path = get_isfileexist_result[0]
    invalid_file_path = get_isfileexist_result[1]
    get_total_filename = len(get_filenames_yaml) #
    get_checksum_def = YAMLUtility.get_checksum(get_server_def[0])
    get_result_rtp = []
    #Diff functions are called to get the filnal result
    all_server_config = YAMLUtility.get_server_config(get_server_def[0],valid_file_path,logger)
    get_compareresult = configcompare(all_server_config)
    get_missing_property = get_compareresult[0]
    get_missing_value = get_compareresult[1]
    get_total_pdv = get_compareresult[2]
    get_checksum_diff = diff_checksum(get_checksum_def,valid_file_path)

    if  len(secondary_server_weblogic) != 0:
        secondary_server_weblogic.update(primary_server_weblogic)
        wlproperty_file_path = propertyFileParser.create_property_file(secondary_server_weblogic,primary_server_weblogic,rtp_file_path,logger)
        get_result_rtp= propertyFileParser.diff_property_File(primary_server_weblogic,wlproperty_file_path)

    create_pdv_result(server_name,pdv_result_path,pdvresult_name,get_total_filename,invalid_file_path,get_missing_property,get_missing_value,get_total_pdv,get_checksum_diff,get_result_rtp)


if __name__ == '__main__':
    # All the attributes are defined here PLEASE DONT CHANGE IT!!!!!!!!
    #change the getfile
    server_name = socket.gethostname()
    main_dir = str(os.environ['HOME'])
    default_path = "/home/weblogic/ConfigDataYAML"#sys.argv[1]
    yaml_folder_path = default_path
    get_timestr = time.strftime("%Y%m%d-%H%M%S")
    logger_name = "TAPDV_"+server_name+"_"+get_timestr+"_log.txt"
    pdvresult_name = "TAPDV_"+server_name+"_"+get_timestr+".txt"
    yaml_file = "ConfigYAML3.yml"#sys.argv[2]
    logger = get_logger(os.path.join(default_path,logger_name))
    primary_Propertyfile_pattern = r'^(Primary)(\_)(\w+)(\.txt)$'
    secondary_Propertyfile_pattern = r'^(Secondary)(\_)(\w+)(\.txt)$'
    #wlproperty_file_path = "/home/cmprime/psPDV/ConfigDataYAML/wlproperty"
    # The type of YAML is passed by the user
    pdv_result_path = default_path
    rtp_file_path = os.path.join(default_path,"rtpfiles")



try:
     main()


except Exception as e:
    print(e)
    logger.error(e)

