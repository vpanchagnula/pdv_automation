import subprocess
import hashlib
import  os
import difflib


# returns output as byte string

# filename(open any file and check the key value pair YAML) and  config check
# printing runtime property check with a diff
# check sum of xmls comparing values with the YAML to xml checksum


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


#  Function returns a list
def create_property_file(serverconfig,folderpath):
    propertyFileList = []
    for eachname, portname in serverconfig.items():
        logger.info("Iterating through server list and creating a property files")
        propertyFileName = "{0}/{1}.txt".format(folderpath,eachname)
        os.popen("execJava com.healthedge.management.client.ConfigurationManagerClient -l t3://{0}:{1}  -c printProperties > {2}".format(eachname,portname,propertyFileName))
        propertyFileList.append(propertyFileName)
    logger.info("Created all the property files based on the server name ")
    return  propertyFileList


# by default all the weblogic01 is primary and then the secondary follows

def diff_property_File(primaryserverpath,secondaryserverspath):
    diff_in_files = []
    #logger.info("Getting primary and secondary paths ")
    if isinstance(secondaryserverspath,list) == True:
        for eachserver in secondaryserverspath:
            diff_subprocess = subprocess.Popen( ['diff', primaryserverpath, eachserver],  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            get_subprocess_output = diff_subprocess.communicate()
            diff_in_files.append(get_subprocess_output[0].decode("UTF-8"))
    else:
        print("Incorrect parameter given")


    return  diff_in_files


def md5(file_name):
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

# Need to shift this to the main script:





try:
    diff_property_File("/home/weblogic/Primary_server01.txt",["/home/weblogic/Secondary_server01.txt","/home/weblogic/Secondary_server02.txt"])


except Exception as e:
        print(e)
